﻿using System;

namespace DemoNoECS
{
    [Serializable]
    public struct Settings
    {
        public int PlayersCount;

        public int FoodCount;

        public int WorldWidth;

        public int WorldHeight;

        public int HalfWorldWidth => WorldWidth / 2;

        public int HalfWorldHeight => WorldHeight / 2;
    }
}