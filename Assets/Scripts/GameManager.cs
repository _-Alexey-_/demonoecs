﻿using DemoNoECS.Controllers;
using System.Collections.Generic;
using UnityEngine;

namespace DemoNoECS
{
    public class GameManager : MonoBehaviour
    {
        #region Fields

        [Header("UI")]
        [SerializeField]
        private GameObject m_UIStartPanel = null;

        [Header("Camera")]
        [SerializeField]
        private CameraController m_CameraController = null;

        [Header("Prefabs")]
        [SerializeField]
        private GameObject m_PlayerPrefab = null;
        [SerializeField]
        private GameObject m_AIPlayerPrefab = null;
        [SerializeField]
        private GameObject m_FoodPrefab = null;

        [Header("Game Settings")]
        [SerializeField]
        Settings m_Settings;

        private static GameManager s_Instance = null;

        private IList<Food> m_Foods = null;
        private IList<BasePlayerController> m_Players = null;

        #endregion Fields

        #region Properties

        public static Settings Settings { get; private set; }

        public static IList<Food> Foods => s_Instance.m_Foods;

        public static IList<BasePlayerController> Players => s_Instance.m_Players;

        #endregion Properties

        #region MonoBehaviour Methods

        private void Awake()
        {
            s_Instance = this;
            Settings = m_Settings;
            m_UIStartPanel.SetActive(true);
        }

        #endregion MonoBehaviour Methods

        #region Public Methods

        public void StartGame()
        {
            SpawnAIPlayers();
            SpawnFoods();
            SpawnPlayer();

            m_CameraController.ResetPosition();
            m_UIStartPanel.SetActive(false);
        }

        public static void Destroy(IEatable eatable)
        {
            switch (eatable)
            {
                case Food food:
                    // Spawn as a new food
                    s_Instance.ResetFood(food);
                    break;

                case AIPlayerController player:
                    // Spawn as a new player
                    s_Instance.ResetAIPlayer(player);
                    break;

                case PlayerController player:
                    // Show Restart screen
                    s_Instance.m_UIStartPanel.SetActive(true);
                    Players.Remove(player);
                    Destroy(player.gameObject);
                    break;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void SpawnAIPlayers()
        {
            Utils.SpawnList(ref m_Players, Settings.PlayersCount, SpawnAIPlayer, ResetAIPlayer);
        }

        private void SpawnFoods()
        {
            Utils.SpawnList(ref m_Foods, Settings.FoodCount, SpawnFood, ResetFood);
        }

        private void SpawnPlayer()
        {
            GameObject player = Instantiate(m_PlayerPrefab);
            Players.Add(player.GetComponent<PlayerController>());

            m_CameraController.Target = player.transform;
        }

        private void SpawnAIPlayer()
        {
            GameObject player = Instantiate(m_AIPlayerPrefab);
            player.GetComponentInChildren<SpriteRenderer>().color = Utils.GetRandomPlayerColor();
            player.transform.SetRandomPosition();

            Players.Add(player.GetComponent<AIPlayerController>());
        }
        private void ResetAIPlayer(BasePlayerController player)
        {
            player.Reset();
        }

        private void SpawnFood()
        {
            GameObject food = Instantiate(m_FoodPrefab);
            food.GetComponent<SpriteRenderer>().color = Utils.GetRandomFoodColor();
            food.transform.SetRandomPosition();

            Foods.Add(food.GetComponent<Food>());
        }

        private void ResetFood(Food food)
        {
            food.transform.SetRandomPosition();
        }

        #endregion Private Methods
    }
}