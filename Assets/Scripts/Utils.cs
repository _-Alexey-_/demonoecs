﻿using System.Collections.Generic;
using UnityEngine;

namespace DemoNoECS
{
    public static class Utils
    {
        public static void SetRandomPosition(this Transform transform)
        {
            transform.position = GetRandomPosition();
        }

        public static Vector3 GetRandomPosition()
        {
            return new Vector3(Random.value * GameManager.Settings.WorldWidth - GameManager.Settings.HalfWorldWidth,
                               Random.value * GameManager.Settings.WorldHeight - GameManager.Settings.HalfWorldHeight, 0);
        }

        public static Color GetRandomPlayerColor() => Color.HSVToRGB(Random.value, 0.7f, 1f);

        public static Color GetRandomFoodColor() => Color.HSVToRGB(Random.value, 0.5f, 1f);

        public static void SpawnList<T>(ref IList<T> list, int length, System.Action spawnAction, System.Action<T> resetAction) where T : MonoBehaviour
        {
            // Delete old arrays if size do not match
            if (list != null && list.Count != length)
            {
                foreach (var go in list)
                    Object.Destroy(go.gameObject);
                list = null;
            }

            if (list == null)
            {
                // Spawn new list
                list = new List<T>();
                for (int i = 0; i < length; i++)
                    spawnAction();
            }
            else
            {
                // Reset already spawn list
                foreach (var go in list)
                    resetAction(go);
            }
        }

        public static float CalculateSpeed(int size) => Mathf.Lerp(Constants.MaxSpeed, Constants.MinSpeed, Mathf.InverseLerp(Constants.MinPlayerSize, Constants.MaxPlayerSize, size));
    }
}
