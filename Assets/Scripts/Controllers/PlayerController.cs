﻿using UnityEngine;

namespace DemoNoECS.Controllers
{
    public class PlayerController : BasePlayerController
    {
        #region Fields

        private Camera m_Camera = null;

        #endregion Fields

        #region MonoBehaviour Methods

        protected override void Start()
        {
            base.Start();

            m_Camera = Camera.main;
        }

        protected override void Update()
        {
            base.Update();

            // Move to the mouse cursor
            Vector3 movement = m_Camera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            movement.z = 0;

            Move(movement);
        }

        #endregion MonoBehaviour Methods
    }
}