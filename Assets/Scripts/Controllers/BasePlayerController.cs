﻿using TMPro;
using UnityEngine;

namespace DemoNoECS.Controllers
{
    public abstract class BasePlayerController : MonoBehaviour, IEatable
    {
        #region Fields

        [SerializeField]
        protected TextMeshPro m_Text = null;
        [SerializeField]
        protected Transform m_Body = null;

        private const float k_RotationRange = 3;
        private const float k_AllowedTurnAngle = 5;

        private float m_RotationSpeed = 0;
        private float m_TimeSinceEat = 0;
        private Vector3 m_Direction = Vector3.one;

        protected float m_Speed = Constants.MaxSpeed;
        protected int m_Size = Constants.InitialPlayerSize;

        #endregion Fields

        #region Properties

        public int Size
        {
            get => m_Size;
            set
            {
                m_Size = Mathf.Clamp(value, Constants.MinPlayerSize, Constants.MaxPlayerSize);
                m_Text.text = m_Size.ToString();
                m_Speed = Utils.CalculateSpeed(m_Size);
                transform.localScale = Vector3.one * Mathf.Sqrt(m_Size) * Constants.SizeToScaleFactor;
            }
        }

        private bool NotEatForLongTime => m_TimeSinceEat > Constants.TimeSinceEat && m_Size > Constants.MinPlayerSize;

        #endregion Properties

        #region MonoBehaviour Methods

        protected virtual void Start()
        {
            Size = m_Size;
            m_RotationSpeed = Random.Range(-k_RotationRange, k_RotationRange);
        }

        /// <summary>
        /// If player do not eat anyone more than <see cref="Constants.TimeSinceEat"/>,
        /// player's size start decreasing by <see cref="Constants.SizeToLose"/> size 
        /// each <see cref="Constants.TimeToIncrementLose"/> time period.
        /// </summary>
        protected virtual void Update()
        {
            m_TimeSinceEat += Time.deltaTime;

            if (NotEatForLongTime)
                DecreaseSize();

            RotateImage();
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            Food food = collision.gameObject.GetComponent<Food>();
            BasePlayerController player = collision.gameObject.GetComponent<BasePlayerController>();

            IEatable eatable = (player != null && Size > player.Size) ? player :
                               food != null ? (IEatable)food : null;

            if (eatable != null)
            {
                Size += eatable.Size;
                GameManager.Destroy(eatable);
                m_TimeSinceEat = 0;
            }
        }

        #endregion MonoBehaviour Methods

        #region Methods

        public virtual void Reset()
        {
            m_TimeSinceEat = 0;
            Size = Constants.InitialPlayerSize;
            transform.SetRandomPosition();
        }

        protected virtual void Move(Vector3 direction)
        {
            // Limit turns to left or right by allowed angle
            if (Vector3.Angle(m_Direction, direction) > k_AllowedTurnAngle)
            {
                var angle = Vector3.SignedAngle(m_Direction, direction, Vector3.back);
                m_Direction = RotateVectorByAngle(m_Direction, angle);
            }
            else
                m_Direction = direction.normalized;

            Vector3 pos = transform.position + m_Direction * m_Speed * Time.deltaTime;

            transform.position = GetPositionInGameBounds(pos);
        }

        private void DecreaseSize()
        {
            Size -= Constants.SizeToLose;
            m_TimeSinceEat -= Constants.TimeToIncrementLose;
        }

        private void RotateImage()
        {
            var rot = m_Body.eulerAngles;
            rot.z += m_RotationSpeed;
            m_Body.eulerAngles = rot;
        }

        private Vector3 RotateVectorByAngle(Vector3 vector, float angle)
        {
            angle = Mathf.Clamp(angle, -k_AllowedTurnAngle, k_AllowedTurnAngle) * Mathf.Deg2Rad;

            return new Vector3(vector.x * Mathf.Cos(angle) + vector.y * Mathf.Sin(angle),
                              -vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle)).normalized;
        }

        private Vector3 GetPositionInGameBounds(Vector3 position)
        {
            float halfSize = transform.localScale.x / 2;
            return new Vector3(Mathf.Clamp(position.x, -GameManager.Settings.HalfWorldWidth + halfSize, GameManager.Settings.HalfWorldWidth - halfSize),
                               Mathf.Clamp(position.y, -GameManager.Settings.HalfWorldHeight + halfSize, GameManager.Settings.HalfWorldHeight - halfSize));

        }

        #endregion Methods
    }
}