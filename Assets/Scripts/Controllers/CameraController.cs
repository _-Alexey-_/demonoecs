﻿using System.Linq;
using UnityEngine;

namespace DemoNoECS.Controllers
{
    public class CameraController : MonoBehaviour
    {
        #region Fields

        private const int k_Scale = 5;

        private Camera m_Camera = null;
        private float m_DesiredCamSize;

        #endregion Fields

        #region Properties

        public Transform Target { get; set; }

        #endregion Properties

        #region MonoBehaviour Methods

        private void Start()
        {
            m_Camera = GetComponent<Camera>();
            m_DesiredCamSize = m_Camera.orthographicSize;
        }

        private void Update()
        {
            if (Target != null)
                FollowTarget();
            else
                FindTarget();
        }

        #endregion MonoBehaviour Methods

        #region Public Methods

        public void ResetPosition()
        {
            transform.position = Vector3.zero;
        }

        #endregion Public Methods

        #region Private Methods

        private void FollowTarget()
        {
            m_DesiredCamSize = Target.localScale.x * k_Scale;
            m_Camera.orthographicSize = Mathf.Lerp(m_Camera.orthographicSize, m_DesiredCamSize, Time.deltaTime);

            Vector3 pos = Vector3.Lerp(transform.position, Target.position, Time.deltaTime);
            pos.z = -1;
            transform.position = pos;
        }

        private void FindTarget()
        {
            if (GameManager.Players != null)
            {
                var player = GameManager.Players.Where(s => s.Size > Constants.MinPlayerSize * 2).FirstOrDefault();
                if (player != null && player is AIPlayerController)
                    Target = player.transform;
            }
        }

        #endregion Private Methods
    }
}