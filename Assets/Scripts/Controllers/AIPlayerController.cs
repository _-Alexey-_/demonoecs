﻿using System.Collections.Generic;
using UnityEngine;

namespace DemoNoECS.Controllers
{
    public class AIPlayerController : BasePlayerController
    {
        #region Fields

        private const float k_MinDistScale = 0.05f;

        private Vector3? m_TargetDirection = null;
        private float m_MinDist = 0;

        #endregion Fields

        #region MonoBehaviour Methods

        protected override void Start()
        {
            base.Start();

            m_MinDist = (GameManager.Settings.WorldHeight + GameManager.Settings.WorldWidth) * k_MinDistScale;
        }

        protected override void Update()
        {
            base.Update();

            m_TargetDirection = null;
            BasePlayerController nearestPlayer = GetNearest(GameManager.Players, out float dist);

            if (nearestPlayer != null && dist < m_MinDist)
            {
                // Move to or move from the nearest player
                m_TargetDirection = (nearestPlayer.transform.position - transform.position) * (Size > nearestPlayer.Size ? 1 : -1);
            }
            else
            {
                Food nearestFood = GetNearest(GameManager.Foods, out _);
                // Move to the nearest food
                m_TargetDirection = nearestFood.transform.position - transform.position;
            }

            if (m_TargetDirection.HasValue)
                Move(m_TargetDirection.Value);
        }

        #endregion MonoBehaviour Methods

        #region Private Methods

        private T GetNearest<T>(IList<T> list, out float distance) where T : MonoBehaviour
        {
            Vector3 pos = transform.position;
            T nearest = null;
            distance = float.MaxValue;

            foreach (var player in list)
            {
                // skip ourself in players list
                if (player == this)
                    continue;

                float d = Vector3.Distance(player.transform.position, pos);
                if (d < distance)
                {
                    distance = d;
                    nearest = player;
                }
            }

            return nearest;
        }

        #endregion Private Methods
    }
}
