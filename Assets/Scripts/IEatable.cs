﻿namespace DemoNoECS
{
    public interface IEatable
    {
        int Size { get; }
    }
}