﻿namespace DemoNoECS
{
    public static class Constants
    {
        public const float MinSpeed = 5;
        public const float MaxSpeed = 20;
        public const int MinPlayerSize = 10;
        public const int MaxPlayerSize = 1000;

        public const int InitialPlayerSize = MinPlayerSize;
        public const int InitialFoodSize = 1;
        public const float InitialSpeed = MaxSpeed;

        public const float TimeSinceEat = 3;
        public const float TimeToIncrementLose = 1;
        public const int SizeToLose = 1;

        public const float SizeToScaleFactor = 2.3f;
    }
}
