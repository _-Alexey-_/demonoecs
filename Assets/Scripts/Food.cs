﻿using UnityEngine;

namespace DemoNoECS
{
    public class Food : MonoBehaviour, IEatable
    {
        #region Properties

        public int Size { get; } = Constants.InitialFoodSize;

        #endregion Properties
    }
}